#+TITLE: Babashka OpenConnect VPN Script
* Blog Post
This is described in detail at This is described in detail at [[https://tech.toryanderson.com/2021/03/06/re-writing-an-openconnect-vpn-connect-script-in-babashka/][https://tech.toryanderson.com/2021/03/06/re-writing-an-openconnect-vpn-connect-script-in-babashka/]]
* Credentials File
The location of your credentials file an be specified by a =-c= option (=~/= paths are supported), or it defaults to looking in =~/.vpncred.edn=. 

The EDN is expected to be have the following structure: 

#+begin_src clojure
  {:username "USER"
   :password "PASS"
   :vpn-server "VPN.COM"
   :authgroup "General_Access"
   :servercert "whatever the cert is"}
#+end_src

400 permission (read-only, only to your user) are recommended.
* Installation
After cloning this repo, and if you have Babashka installed on your path, simply put vpn.clj somewhere on your path. I put mine in =~/bin/vpn=, removing the extension and marking it as executible with =chmod +x vpn=. 
* Usage
Usage help is available if you simply call invoke vpn.clj with no args. In particular you have the =start=, =stop=, =status=, and =restart= arguments.
